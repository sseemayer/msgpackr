#include <Rcpp.h>
#include <msgpack.hpp>

using namespace Rcpp;


template <typename A, typename B> SEXP listToVector(List d) {

	std::vector<A> res(d.size());
	for(int i = 0; i < d.size(); i++) {
		res[i] = as<A>(d[i]);
	}

	B resR = wrap(res);

	List names = d.attr("names");
	if(names.size() > 0) {
		resR.attr("names") = names;
	}

	return resR;
}


SEXP msgpackUnifyVectors(List d) {

	SEXP out = d;

	// scan element types
	int commonType = -1;
	for( List::iterator it = d.begin(); it != d.end(); ++it ) {
		int type = TYPEOF(*it);
		size_t size = LENGTH(*it);
		if(commonType == -1) { commonType = type; }
		else if(type != commonType) { commonType = -2; break; }
		else if(size > 1) { commonType = -2; break; }
	}

	if(commonType == INTSXP) {
		out = listToVector<int, IntegerVector>(d);
	} else if(commonType == REALSXP) {
		out = listToVector<double, NumericVector>(d);
	} else if(commonType == LGLSXP) {
		out = listToVector<bool, LogicalVector>(d);
	} else if(commonType == STRSXP) {
		out = listToVector<std::string, StringVector>(d);
	} else if(commonType = VECSXP) {
		// don't unify vectors of vectors
	} else if(commonType != -2) {
		printf("Unknown homogeneous type: %d\n", commonType);
	}

	return out;
}

SEXP msgpackToR(msgpack::object obj, bool unifyVectors) {

	SEXP out = NULL;
	std::vector<SEXP> lst;
	std::map<std::string, SEXP> map;

	switch(obj.type) {

		case msgpack::type::NIL:
			out = R_NilValue;
			break;

		case msgpack::type::BOOLEAN:
			out = wrap(obj.as<bool>());
			break;

		case msgpack::type::POSITIVE_INTEGER:
		case msgpack::type::NEGATIVE_INTEGER:
			out = wrap(obj.as<int>());
			break;

		case msgpack::type::FLOAT:
			out = wrap(obj.as<double>());
			break;

		case msgpack::type::STR:
			out = wrap(obj.as<std::string>());
			break;

		case msgpack::type::ARRAY:
			if(obj.via.array.size != 0) {
				msgpack::object* p(obj.via.array.ptr);
				for(msgpack::object* const pend(obj.via.array.ptr + obj.via.array.size); p < pend; ++p) {
					SEXP inner_obj = PROTECT(msgpackToR(*p, unifyVectors));
					lst.push_back(inner_obj);
				}

				out = wrap(lst);

				if(unifyVectors) {
					out = msgpackUnifyVectors(out);
				}

				UNPROTECT(obj.via.array.size);
			}
			break;

		case msgpack::type::MAP:
			if(obj.via.map.size != 0) {
				msgpack::object_kv* p(obj.via.map.ptr);
				for(msgpack::object_kv* const pend(obj.via.map.ptr + obj.via.map.size); p < pend; ++p) {

					std::string k = p->key.as<std::string>();
					SEXP inner_v = PROTECT(msgpackToR(p->val, unifyVectors));
					map[k] = inner_v;
				}

				out = wrap(map);

				if(unifyVectors) {
					out = msgpackUnifyVectors(out);
				}

				UNPROTECT(obj.via.map.size);

			}
			break;

		default:
			std::cout << "UNKNOWN" << std::endl;
			break;
	}

	return out;
}

// [[Rcpp::export]]
SEXP unpackC(RawVector data) {

	char* buf = (char*)malloc(sizeof(char) * data.size());
	memcpy(buf, data.begin(), data.size());
	size_t len = data.size();

	msgpack::unpacked msg;
	msgpack::unpack(&msg, buf, len);

	msgpack::object obj = msg.get();

	SEXP out = msgpackToR(obj, true);

	free(buf);

	return out;
}


